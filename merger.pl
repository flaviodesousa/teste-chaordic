use 5.012;

use strict;
use warnings FATAL => 'all';

use threads;
use threads::shared;

use Getopt::Long;
use File::Spec;
use FileHandle;
use IO::Socket;

use Lib::Chaordic ':all';

use constant MAXUSERIDS => 1000;
use constant WORKERS => 10;

our ($opt_verbose, $opt_mergelist, $opt_intermediate_dir,
	 $opt_target_dir, $opt_host, $opt_nondestructive);
our @workers;
our @userids :shared;
our $done :shared;

sub get_socket {
	my ($host) = @_;
	my $h = IO::Socket::INET->new( PeerAddr => $host,
								   PeerPort => DEFAULT_PORT,
								   Proto    => "tcp");
	die "unable to connect to $host: $!" unless $h;
	return $h;
}

sub merge_files {
	my ($task) = @_;
	unless ($task =~ /(?<userid>.+?):(?<files>.+)$/) {
		die "Host $opt_host: Badly formed task row '$task'";
	}
	my ($userid, @files) = ($+{userid}, split /:/,$+{files});
	my $target_fh;

	{
		my $fulltarget = File::Spec->join($opt_target_dir, "$userid.log");
		$target_fh = FileHandle->new($fulltarget, "w")
			or die "$fulltarget: $!";
	}

	my @filelist;

	for (@files) {
		my ($host, $filename) = split /;/;
		my $fullname = File::Spec->join($opt_intermediate_dir, $filename);
		my ($h, $handle_data);
		if ($host eq $opt_host) {
			$handle_data = { type => 'file' };
			$h = FileHandle->new($fullname)
				or die "$fullname: $!";
		} else {
			$handle_data = { type => 'socket', host => $host };
			$h = get_socket($host);
			$h->print("GET $fullname\n")
				or die "sending get cmd: $!";
		}

		push @filelist, fetch_source_row { handle => $h,
										   path => $fullname,
										   handle_data => $handle_data };
	}

	while (@filelist) {
		my ($best, $s);
		for (my $i = 0; $i < @filelist; $i++) {
			if (not $best or $best->{time} gt $filelist[$i]->{time}) {
				$s = $i;
				$best = $filelist[$s];
			}
		}

		$target_fh->print($best->{nextrow});

		if ($best->{handle}->eof) {
			$best->{handle}->close();
			unless ($opt_nondestructive) {
				given ($best->{handle_data}->{type}) {
					when ('file') {
						unlink $best->{path};
					}
					when ('socket') {
						get_socket($best->{handle_data}->{host})->print("KILL $best->{path}\n");
					}
				}
			}
			splice @filelist, $s, 1;
		} else {
			fetch_source_row $best;
		}
	}
	$target_fh-close();
}

sub dequeue_tasks {
	for (;;) {
		my $task;
		{
			lock(@userids);
			while (not @userids) {
				threads->exit() if $done;
				cond_wait @userids;
			}
			$task = pop @userids;
			# signals to fetch more tasks
			cond_signal @userids;
		}
		merge_files $task;
	}
}

sub enqueue_tasks {
	my ($mergelist) = @_;
	my $fh = FileHandle->new;
	$fh->open($mergelist) or die "$!\n";
	while (<$fh>) {
		chomp;
		my $task = $_;
		{
			lock(@userids);
			push @userids, $task;
			# signals there is a new task
			cond_signal @userids;
			# pauses if too many lines read
			cond_wait @userids if @userids >= MAXUSERIDS;
		}
	}
	$fh->close;
	$done = 1;
	{
		lock(@userids);
		cond_broadcast @userids;
	}
}

GetOptions ("verbose"        => \$opt_verbose,
			"nondestructive" => \$opt_nondestructive,
            "mergelist=s"    => \$opt_mergelist,
			"host=s"         => \$opt_host,
            "target=s"       => \$opt_target_dir,
            "intermediate=s" => \$opt_intermediate_dir)
	or die "Bad arguments\n";
die "Missing --target argument\n" unless $opt_target_dir;
die "Missing --intermediate argument\n" unless $opt_intermediate_dir;
die "Missing --mergelist argument\n" unless $opt_mergelist;
die "Missing --host argument\n" unless $opt_host;
die "$opt_intermediate_dir dir does not exist\n" unless -d $opt_intermediate_dir;
die "$opt_target_dir dir does not exist\n" unless -d $opt_target_dir;
die "$opt_mergelist file does not exist\n" unless -f $opt_mergelist;

push(@workers, threads->create(\&dequeue_tasks)) for 1..(WORKERS);

enqueue_tasks($opt_mergelist);

say "Waiting for threads to finish" if $opt_verbose;
(pop @workers)->join while @workers;

say "All files merged" if $opt_verbose;