use 5.012;

use strict;
use warnings FATAL => qw( all );

use Cwd 'abs_path';
use FileHandle;
use File::Basename;
use File::Spec;
use Getopt::Long;
use IO::Socket;

use Lib::Chaordic ':all';

our ($opt_verbose, $opt_master, $opt_port, $opt_filelist,
	 $opt_intermediate, $opt_target, $opt_nondestructive);

sub rotate {
	my ($array) = @_;
	my $head = shift @$array;
	push @$array, $head;
	return $head;
}

sub collect_hosts {
	my ($filelist) = @_;
	my (%hosts, @hosts);
	my $fh = FileHandle->new;
	my $localbase = dirname(abs_path($0));
	my $port = DEFAULT_PORT;
	say "Opening filelist $filelist" if $opt_verbose;
	$fh->open($filelist) or die "$!\n";
	while (<$fh>) {
		chomp;
		my ($host, $file) = split /:/;
		next if $hosts{$host};
		# new host
		$hosts{$host} = 1;
		push @hosts, $host;
		if (fork == 0) {
			say "Setting up host $host..." if $opt_verbose;
			system("ssh", $host, "mkdir -p /tmp/teste-chaordic/Lib");
			system("scp", '-qB',
			              $filelist,
						  File::Spec->join($localbase, 'killer_server.pl'),
			              File::Spec->join($localbase, 'splitter.pl'),
			              File::Spec->join($localbase, 'merger.pl'),       "$host:/tmp/teste-chaordic");
			system("scp", '-qB',
				          File::Spec->join($localbase, 'Lib/Chaordic.pm'),
			              File::Spec->join($localbase, 'Lib/MRU.pm'),      "$host:/tmp/teste-chaordic/Lib");
			system("ssh", $host, qq{cd /tmp/teste-chaordic;perl killer_server.pl --host $host --port $port --daemon});
			say "Host $host was set up" if $opt_verbose;
			exit (0);
		}
	}
	$fh->close;
	say "Setting up hosts..." if $opt_verbose;
	while (wait >= 0) {
		say "...one host was set up" if $opt_verbose;
	}
	say "All hosts were set up" if $opt_verbose;
	return @hosts;
}

sub split_merge {
	my (@hosts) = @_;
	my $base_port = $opt_port;
	my $base_filelist = basename($opt_filelist);
	for my $host (@hosts) {
		if (fork == 0) {
			system("ssh",$host,
				qq{cd /tmp/teste-chaordic;}.
				qq{mkdir -p $opt_intermediate;}.
				qq{find $opt_intermediate -name "*.split" -delete;}.
				qq{perl splitter.pl }.
					qq{--host=$host --filelist=$base_filelist }.
					qq{--intermediate=$opt_intermediate;}.
				qq{(ls $opt_intermediate > /tmp/teste-chaordic/$host.split.list)});
			system("scp", '-qB',
				"$host:/tmp/teste-chaordic/$host.split.list",
				"/tmp");
			exit(0);
		}
		say "Started split/merge on $host" if $opt_verbose;
	}
	say "Waiting..." if $opt_verbose;
	while (wait >= 0) {
		say "...one task finished..." if $opt_verbose;
	}
	say "All splitter tasks finished!" if $opt_verbose;
}

sub distribute_mergers {
	my (@hosts) = @_;
	my (%merge_lists, $host);
	say "Distributing lists of split files among hosts..." if $opt_verbose;
	for $host (@hosts) {
		$merge_lists{$host} = FileHandle->new;
		$merge_lists{$host}->open("/tmp/$host.merge.list", "w")
			or die "merge list for $host: $!\n";
	}
	my $splits = join ' ', map { "/tmp/$_.split.list" } @hosts;
	open SPLITS, "sort $splits|";
	my $last_userid = "";
	while (<SPLITS>) {
		chomp;
		my ($useridfilename) = $_;
		if ($useridfilename =~ qr{^\[(?<userid>.+)\]\[(?<host>.+)\].split$}) {
			# distribute userids by round robin hosts
			if ($+{userid} ne $last_userid) {
				$merge_lists{$host}->print("\n") if $last_userid;
				$host = rotate \@hosts;
				$last_userid = $+{userid};
				$merge_lists{$host}->print("$last_userid");
			}
			$merge_lists{$host}->print(":$+{host};$useridfilename");
		}
	}
	while (wait >= 0) {
		say "Split file list sorter finished" if $opt_verbose;
	}
	say "Sending lists for final merging..." if $opt_verbose;
	my $nondestructive = $opt_nondestructive ? "--nondestructive" : "";
	for $host (@hosts) {
		if (fork == 0) {
			$merge_lists{$host}->close;
			system("scp", '-qB',
				"/tmp/$host.merge.list","$host:/tmp/teste-chaordic/");
			unlink("/tmp/$host.split.list", "/tmp/$host.merge.list")
				unless $opt_nondestructive;
			system("ssh",$host,
				qq{cd /tmp/teste-chaordic;}.
				qq{mkdir -p $opt_target;}.
				qq{find $opt_target -name "*.log" -delete;}.
				qq{perl merger.pl }.
					qq{--host $host }.
					qq{--mergelist=/tmp/teste-chaordic/$host.merge.list }.
					qq{--intermediate=$opt_intermediate }.
					qq{--target=$opt_target $nondestructive});
			exit(0);
		}
	}
	say "Waiting for all merges to finish..." if $opt_verbose;
	while (wait >= 0) {
		say "...one merge finished..." if $opt_verbose;
	}
	say "Terminanting socket file servers..." if $opt_verbose;
	for $host (@hosts) {
		my $h = IO::Socket::INET->new( PeerAddr => $host,
									   PeerPort => DEFAULT_PORT,
									   Proto    => "tcp")
					or die "unable to connect to $host: $!";
		$h->print("DIE\n");
		$h->close();
	}
	say "All done!" if $opt_verbose;
}

GetOptions ("verbose" => \$opt_verbose,
			"nondestructive" => \$opt_nondestructive,
	        "filelist=s" => \$opt_filelist,
	        "intermediate=s" => \$opt_intermediate,
	        "target=s" => \$opt_target)
    or die "Bad arguments\n";
die "Missing --target argument\n" unless $opt_target;
die "Missing --intermediate argument\n" unless $opt_intermediate;
die "Missing --filelist argument\n" unless $opt_filelist;
die "$opt_filelist does not exist" unless -f $opt_filelist;

my @hosts = collect_hosts($opt_filelist);
split_merge(@hosts);
distribute_mergers(@hosts);