To run the whole thing:

    perl chaorchestrator.pl --filelist /home/flavio/chaordic/listsmall.in --verbose --intermediate=/tmp/intermediate --target=/tmp/target

To count the generated files lines and chars (must match source and result files):

	find /tmp/chaordic/target -iname "*log" -printf "%p\0"|wc --files0-from=-

The 1st stage split/merge command:

	time perl splitter.pl --host=4 --filelist /tmp/chaordic/filelist.in --target /tmp/chaordic/target

To generate test data:

	mkdir /tmp/chaordic; cd /tmp/chaordic; mkdir 1 2 3 4; cd -
	time perl testgen/testgen.pl /tmp/chaordic