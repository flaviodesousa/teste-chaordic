Teste backend Chaordic
======================

Premissas e Suposições
----------------------

- Os arquivos a serem lidos tem "vários terabytes"
- Algumas linhas incluem um cookie "userid" (posso inferir que TODAS linhas possuem tal cookie, mas isso é indiferente diante das demais incertezas)
- Somente as linhas que contem o cookie userid serão consideradas
- Cada arquivo está ordenado pelo tempo
- Posso ter vários userid diferentes
- Há 4 servidores, cada um com endereço IP distinto
- Há diversos arquivos em cada servidor
- Cada servidor pode ter apenas 1 arquivo gigante ou poucas centenas de arquivos grandes
- Como o userid se refere, pelo nome, aos acessos ligados a um usuário presume-se que estejam temporalmente agregados, ou seja: sessões
- Como não foi especificado quais são os arquivos origem eles terão que ser informados 1 a 1 no padrão "host:path"
- Há dezenas de arquivos por servidor (poucas centenas, *no máximo*), sendo possível a um único processo ter todos abertos simultaneamente
- Os *hosts* estão todos dentro de uma mesma rede segura, não necessitando medidas adicionais de segurança

Setup
-----

*Application tested on a virtual cluster made of a Ubuntu 12.04 master and Debian 7.1 slaves*

Each host will need to have:

* Perl 5.12 (or greater)
* SSH with all hosts trusting each other (see `.ssh/authorized_keys`)

Syntax
------

    perl chaorchestrator --filelist <file-list> --intermediate <intermediate-dir> --target <target-dir> [--verbose]

   `--verbose` enables status messages during the process

   `--intermediate` temporary directory where split/merge will create files

   `--target` where the final resulting files will be placed

   `--filelist` list of files that will be processed. The format each row of the list is <host>:<full path>, as below:

  Sample for `--filelist`:

    $ cat sample-list.txt
    10.1.1.10:/tmp/file1.log
    10.1.1.10:/var/log/nginx.log
    10.1.1.11:/var/chaordic/log/20130825.log

Workflow
--------

1. Build the file list

2. Start *chaorchestrator* providing the source files listing, a common intermediate and target directories (that will be created on each host)

3. chaorchestrator starts `killer_server.pl` on each host

4. chaorchestrator starts `splitter.pl` on each host appearing in the file list. This will create *userid* separated files on `--intermediate`

5. chaorchestrator waits for all splitter tasks to finish

6. chaorchestrator gathers all resulting `intermediate` files listiings and build final target listings balancing the amount of files among hosts

7. chaorchestrator fires `merger.pl` on each host appearing in the file list, and they will spawn several workers. Each worker will process whole lines from the `--intermediate` directories, generating final `--target` files. They will read local files from filesystem and remote files will be read by tcp socket usign the `killer_server.pl` provider. Note: `--intermediate` files, local and remote, are deleted after read.

8. chaorchestrator waits for all merger tasks to finish

9. chaorchestrator stops all `killer_server.pl`, sending them a `DIE` command

10. Process finishes with each *host* having a set of *userid* separated file with all time ordered ocurrences of the *userid* among all hosts