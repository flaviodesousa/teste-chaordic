use 5.012;

use strict;
use warnings;

use FileHandle;

use Lib::Chaordic ':all';

sub main {
	my ($filename) = @_;

	my $fh = FileHandle->new($filename)
		or die "$filename: $!";

	my $previous = parse_line scalar <$fh>;

	die "$previous->{userid} not part of the filename"
		unless -1 < index $filename, $previous->{userid};

	while (<$fh>) {
		my $current = parse_line $_;
		die "not sorted at $. before=$previous->{time} after=$current->{time}"
			if $previous->{time} gt $current->{time};
		die "unexpected userid $current->{userid}"
			if $current->{userid} ne $previous->{userid};
		$previous = $current;
	}

	$fh->close();
}

main($_) for (@ARGV);

exit(0);