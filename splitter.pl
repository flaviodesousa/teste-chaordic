use 5.012;

use strict;
use warnings FATAL => 'all';

use Getopt::Long;
use FileHandle;

use Lib::Chaordic ':all';
use Lib::MRU;

our ($opt_filelist, $opt_verbose, $opt_intermediate_dir, $opt_host);

sub open_source_files {
	my $flh = FileHandle->new;
	$flh->open($opt_filelist) or die "$!\n";
	my @openfiles;
	while (<$flh>) {
		chomp;
		my ($host, $file) = split /:/;

		next unless $host eq $opt_host;

		my $fh = FileHandle->new($file)
			or die "$file: $!\n";

		if ($fh->eof) {
			warn "$file is empty! Discarded.\n"
				if $opt_verbose;
			$fh->close;
			next;
		}

		push @openfiles, fetch_source_row { handle => $fh, path => $file };
	}
	$flh->close;

	return @openfiles;
}

sub split_merge {
	my @openfiles = @_;
	my $mru = Lib::MRU->new($opt_host, $opt_intermediate_dir);
	while (@openfiles) {
		my ($best, $s);
		for (my $i = 0; $i < @openfiles; $i++) {
			if (not $best or $best->{time} gt $openfiles[$i]->{time}) {
				$s = $i;
				$best = $openfiles[$s];
			}
		}

		$mru->select($best)->{handle}->print($best->{nextrow});

		if ($best->{handle}->eof) {
			say "file finished: $best->{path}"
				if $opt_verbose;
			$best->{handle}->close();
			splice @openfiles, $s, 1;
		} else {
			fetch_source_row $best;
		}
	}
}

GetOptions ("verbose" => \$opt_verbose,
			"host=s" => \$opt_host,
            "filelist=s" => \$opt_filelist,
            "intermediate=s" => \$opt_intermediate_dir)
	or die "Bad arguments\n";
die "Missing --intermediate argument\n" unless $opt_intermediate_dir;
die "Missing --filelist argument\n" unless $opt_filelist;
die "Missing --host argument\n" unless $opt_host;
die "$opt_filelist does not exist" unless -f $opt_filelist;

my @openfiles = open_source_files();
split_merge(@openfiles);
