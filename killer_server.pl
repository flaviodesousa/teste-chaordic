use 5.012;

use strict;
use warnings FATAL => 'all';

use Getopt::Long;
use FileHandle;
use IO::Socket::INET;
use POSIX 'setsid';
use threads;

our ($opt_host, $opt_port, $opt_verbose, $opt_daemon);

sub talker {
	my ($client) = @_;
	my $command = $client->getline;
	chomp $command;
	$client->shutdown(0); #stopped reading
	say qq(command=$command) if $opt_verbose;
	if ($command =~ /(?<verb>\w+)(?:\s+(?<subject>\S+))?/) {
		my ($verb, $subject);
		$verb = uc $+{verb};
		$subject = $+{subject};

		given ($verb) {
			when ('DIE') {
				say "Suiciding" if $opt_verbose;
				$client->shutdown(2);
				$client->close;
				exit(0);
			}
			when ('GET') {
				say "Sending file $subject" if $opt_verbose;
				my $fh = FileHandle->new($subject);
				unless ($fh) {
					$client->print("Error: $!\n");
					die "$subject: $!\n";
				}
				while (<$fh>) {
					$client->print($_);
				}
				$fh->close;
				$client->shutdown(2);
				$client->close;
			}
			when ('KILL') {
				say "Unlinking file $subject" if $opt_verbose;
				unlink $subject;
				$client->shutdown(2);
				$client->close;
			}
			default {
				$subject = "<nothing>" unless defined $subject;
				say qq(Unknown command "$verb" over "$subject") if $opt_verbose;
				$client->print("$verb?\n");
				$client->shutdown(2);
				$client->close;
			}
		}
	}

	threads->exit();
}

GetOptions("host=s" => \$opt_host,
	       "port=i" => \$opt_port,
	       "daemon" => \$opt_daemon,
	       "verbose" => \$opt_verbose)
	or die "Bad arguments\n";

die "Argument missing --host <host ip>\n" unless $opt_host;
die "Argument missing --port <port#>\n" unless $opt_port;

$| = 1; # auto flush

my ($socket);

$socket = IO::Socket::INET->new (LocalAddr => $opt_host,
								 LocalPort => $opt_port,
								 Proto => 'tcp',
								 Listen => 100,
								 ReuseAddr => 1)
	or die "Socket new failed: $!";

if ($opt_daemon) {
	umask 0;
	open STDIN,  '/dev/null' or die "Can't read /dev/null: $!";
	open STDOUT, '>/dev/null';
	open STDERR, '>/dev/null';
	if (fork > 0) {
		exit 0;
	}
	setsid;
}

for (;;) {
	say "Waiting for connections" if $opt_verbose;

	my $client = $socket->accept();

	say "New connection" if $opt_verbose;

	threads->create(\&talker, $client)->detach;
}
