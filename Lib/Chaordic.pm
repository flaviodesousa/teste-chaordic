package Lib::Chaordic;

use 5.010;

use strict;
use warnings;

use constant MONTHS => {
	Jan =>  1, Feb =>  2, Mar =>  3,
	Apr =>  4, May =>  5, Jun =>  6,
	Jul =>  7, Aug =>  8, Sep =>  9,
	Oct => 10, Nov => 11, Dec => 12 };
use constant DEFAULT_PORT => 25800;

use base 'Exporter';
our @EXPORT_OK = qw(parse_line fetch_source_row DEFAULT_PORT);
our %EXPORT_TAGS = (
	'all' => \@EXPORT_OK
	);

sub parse_line {
	my ($row) = @_;
	return undef
		unless $row =~ qr{\[
			             (?<day>\d{1,2})/
		                 (?<month>\w{3})/
		                 (?<year>\d{4}):
		                 (?<hour>\d{1,2}):
		                 (?<min>\d{1,2}):
		                 (?<sec>\d{1,2})
		                 \ (?<tz>[-+]\d{4}).*
		                 "userid=(?<userid>[-0-9a-f]{36})"$}x;
	return {
		time => sprintf("%04d-%02d-%02d:%02d:%02d:%02d",
		                 $+{year},
		                      MONTHS->{$+{month}},
		                           $+{day},
		                                $+{hour},
		                                     $+{min},
		                                          $+{sec}),
		userid => $+{userid}};
}

sub fetch_source_row {
	my ($source) = @_;
	my $nextrow = $source->{handle}->getline;
	my $r = parse_line $nextrow;
	$source->{nextrow} = $nextrow;
	$source->{time} = $r->{time};
	$source->{userid} = $r->{userid};
	return $source;
}

1;