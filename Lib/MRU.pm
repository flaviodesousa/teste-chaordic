package Lib::MRU;

use 5.012;

use strict;
use warnings FATAL => qw( all );

use FileHandle;
use File::Spec;

use constant MRUSIZE => 100;

sub new {
	my ($class, $host, $target_dir) = @_;
	return bless {
		mru_list => [],
		userids => {},
		host => $host,
		target_dir => $target_dir
	}, $class;
}

sub select {
	my ($self, $source) = @_;
	my $userid = $source->{userid};
	my $mru = $self->{mru_list};
	if ($self->{userids}->{$userid}) {
		# remembered userid
		my $r;
		for ($r = 0; $r < @$mru; $r++) {
			last if $mru->[$r]->{userid} eq $userid;
		}
		if ($r == 0) {
			# great! already the top
			return $mru->[$r];
		}
		# move to the top
		unshift @$mru, my $found = splice @$mru, $r, 1;;
		return $found;
	} else {
		# not remembered userid
		if (@$mru >= MRUSIZE) {
			# forget lru if full
			my $lru = pop @$mru;
			$lru->{handle}->close();
			delete $self->{userids}->{$lru->{userid}};
		}

		my $fh = FileHandle->new;
		my $userid_file = File::Spec->join($self->{target_dir}, "[$userid][$self->{host}].split");
		$fh->open($userid_file, ">>")
			or die "$!\n";
		my $new = { userid => $userid, handle => $fh };
		unshift @$mru, $new;
		$self->{userids}->{$userid} = $new;

		return $new;
	}
}

1;