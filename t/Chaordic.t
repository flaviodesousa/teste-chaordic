use 5.010;

use strict;
use warnings;
use Test::More;

use Lib::Chaordic ':all';

my ($testrow, $parsed);

$testrow = qq|177.126.180.83 - - [15/Aug/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-08-15:14:54:38', 'Date parsed as sortable string';
is $parsed->{userid}, q{5352b590-05ac-11e3-9923-c3e7d8408f3a}, 'Userid extracted';

$testrow = qq|177.126.180.83 - - [15/Jan/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-01-15:14:54:38', 'Date parsed as sortable string - January should be 01';

$testrow = qq|177.126.180.83 - - [15/Feb/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-02-15:14:54:38', 'Date parsed as sortable string - February should be 02';

$testrow = qq|177.126.180.83 - - [15/Mar/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-03-15:14:54:38', 'Date parsed as sortable string - March should be 03';

$testrow = qq|177.126.180.83 - - [15/Apr/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-04-15:14:54:38', 'Date parsed as sortable string - Aril should be 04';

$testrow = qq|177.126.180.83 - - [15/May/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-05-15:14:54:38', 'Date parsed as sortable string - May should be 05';

$testrow = qq|177.126.180.83 - - [15/Jun/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-06-15:14:54:38', 'Date parsed as sortable string - June should be 06';

$testrow = qq|177.126.180.83 - - [15/Jul/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-07-15:14:54:38', 'Date parsed as sortable string - July should be 07';

$testrow = qq|177.126.180.83 - - [15/Aug/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-08-15:14:54:38', 'Date parsed as sortable string - August should be 08';

$testrow = qq|177.126.180.83 - - [15/Sep/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-09-15:14:54:38', 'Date parsed as sortable string - September should be 09';

$testrow = qq|177.126.180.83 - - [15/Oct/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-10-15:14:54:38', 'Date parsed as sortable string - October should be 10';

$testrow = qq|177.126.180.83 - - [15/Nov/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-11-15:14:54:38', 'Date parsed as sortable string - November should be 11';

$testrow = qq|177.126.180.83 - - [15/Dec/2013:14:54:38 -0300] "GET /meme.jpg HTTP/1.1" 200 2148 "-" "userid=5352b590-05ac-11e3-9923-c3e7d8408f3a"|;
$parsed = parse_line $testrow;
is $parsed->{time}, '2013-12-15:14:54:38', 'Date parsed as sortable string - December should be 12';

done_testing;