use 5.010;

use strict;
use warnings;
use Test::More;

require 'testgen.pl';

like testgen::gen_userid(), qr/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/, "userid follows pattern";
like testgen::gen_ip(), qr/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/, "Sane ip";

done_testing;