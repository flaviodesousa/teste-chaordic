package testgen;

use 5.010;

use strict;
use warnings;

use POSIX;
use FileHandle;

use constant MAXUSERS => 100_000;
use constant MAXSESSIONS => 1_000;
use constant MINROWS => 100_000;
use constant MAXROWS => 200_000;

our (@userids, @sessions, %sessions);

sub gen_userid {
	sprintf("%04x%04x-%04x-%04x-%04x-%04x%04x%04x",
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)),
		int(rand(65536)));
}

sub gen_ip {
	my ($net);
	for (;;) {
		$net = int(rand(222)) + 1;
		next if $net == 10 || $net == 172 || $net == 192;
		return sprintf("%d.%d.%d.%d",
			$net,
			int(rand(256)),
			int(rand(256)),
			int(rand(254)) + 1);
	}
}

sub gen_users {
	for (my $i = 0; $i < MAXUSERS; $i++) {
		push @userids, gen_userid();
	}
}

sub open_session {
	return if MAXSESSIONS <= @sessions;
	for (;;) {
		my $user = $userids[int(rand()*MAXUSERS)];
		next if $sessions{$user};
		push @sessions, $user;
		$sessions{$user} = gen_ip();
		return;
	}
}

sub close_session {
	return unless @sessions;
	my $el = int(rand()*@sessions);
	my $user = $sessions[$el];
	splice @sessions, $el, 1;
	delete $sessions{$user};
}

sub gen_rows {
	my ($fn) = @_;
	my $rows = MINROWS + int((MAXROWS - MINROWS) * rand());
	my $time = 1377000000;

	my $fh = FileHandle->new;
	say "Generating $rows rows for $fn";
	$fh->open("> $fn") or die "$!";

	while ($rows > 0) {
		open_session() if rand() < 1 - @sessions / MAXSESSIONS;
		next unless @sessions;
		$time += int(rand() * rand() * 30 * (1 - @sessions / MAXSESSIONS));
		my $el = int(rand()*@sessions);
		my $user = $sessions[$el];
		my $ip = $sessions{$user};
		my $tm = strftime "%d/%b/%Y:%T %z", localtime $time;
		$fh->print("$ip - - [$tm]", ' "GET /irrelevant.gif HTTP/1.1" 200 2148 "-" "userid=',$user,'"',"\n");
		close_session() if rand() < @sessions / MAXSESSIONS;
		$rows--;
	}
	$fh->close();
}

sub run {
	my ($pid, $fn, $server, $i);
	say "Generating users...";
	gen_users();
	say "Generating processes...";
	for $server (1..4) {
		for my $i (1..10) {
			$fn = int(rand() * 100_000_000_000);
			if (fork == 0) {
				gen_rows "$ARGV[0]/$server/$fn.log";
				exit(0);
			}
		}
	}
	do {
		say "Waiting...";
		$pid = wait;
		say "Subprocess ",wait, " finished";
		} while ($pid >= 0);
	exit 0;
}

run unless caller;

1;